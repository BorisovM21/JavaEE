package org.itstep;

import java.sql.*;
import java.util.Scanner;

public class Application {

    private static final String CONNECTION_STRING = "jdbc:mysql://localhost:3306/academy1" +
            "?characterEncoding=UTF-8&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static final String USER = "root";
    private static final String PASS = "1111";

    private static final String DROP_STUDENT = "DROP TABLE IF EXISTS student\n";
    private static final String DROP_GROUP = "DROP TABLE IF EXISTS `group`\n";

    private static final String CREATE_STUDENT = "CREATE TABLE IF NOT EXISTS student\n" +
            "(\n" +
            "   id INT PRIMARY KEY AUTO_INCREMENT,\n" +
            "   first_name VARCHAR(255) NOT NULL,\n" +
            "   last_name VARCHAR(255) NOT NULL,\n" +
            "   age int,\n" +
            "   email VARCHAR(255) NOT NULL UNIQUE,\n" +
            "   group_id int,\n" +
            "   FOREIGN KEY (group_id) REFERENCES `group`(id)," +
            "   CONSTRAINT first_last_unique UNIQUE (first_name, last_name)\n" +
            ")";

    private static final String CREATE_GROUP = "CREATE TABLE IF NOT EXISTS `group`\n" +
            "(\n" +
            "   id INT PRIMARY KEY AUTO_INCREMENT,\n" +
            "   name VARCHAR(255) NOT NULL UNIQUE\n" +
            ")\n";

    private static final String ALTER_STUDENT = "ALTER TABLE student \n" +
            "ADD CONSTRAINT student_group_fk FOREIGN KEY (group_id) REFERENCES `group`(id)\n";

    public static void main(String[] args) {
        boolean start = true;
        while (start) {
            try (Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, PASS)) {
                Statement stmt = conn.createStatement();
                Scanner scanner = new Scanner(System.in);
                System.out.print("Введите порядковый номер действия которое вы хотите совершить: \n" +
                        "\t1. Добавить группу\n" +
                        "\t2. Добавить студента\n" +
                        "\t3. Вывести список студентов\n" +
                        "\t4. Вывести список групп\n" +
                        "\t5. Выход: ");
                int read = scanner.nextInt();
                if (read == 1) {
                    System.out.println("Добавление новой группы");
                    newGroup(stmt);
                } else if (read == 2) {
                    System.out.println("Добавление нового студента");
                    newStudent(stmt);
                } else if (read == 3) {
                    System.out.println("Список всех студентов: ");
                    getStudent(stmt);
                } else if (read == 4) {
                    System.out.println("Список груп:");
                    getGroups(stmt);
                } else if (read == 5) {
                    start = false;
                    System.out.println("Програма завершена");
                }
                //initDatabase(stmt);
                //newGroup(stmt);
                //newStudent(stmt);
                //getStudent(stmt);
                //getGroups(stmt);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void getGroups(Statement stmt) throws SQLException {
        ResultSet result = stmt.executeQuery("SELECT * FROM `group`");
        while (result.next()) {
            System.out.format("%d\t%s%n", result.getInt(1),
                    result.getString("name"));
        }
    }

    private static void getStudent(Statement stmt) throws SQLException {
        ResultSet result = stmt.executeQuery("SELECT * FROM student");
        while (result.next()) {
            System.out.format("%s%n%s%n%d%n%s%n%d%n",
                    result.getString("first_name"),
                    result.getString("last_name"),
                    result.getInt("age"),
                    result.getString("email"),
                    result.getInt("group_id"));
            System.out.println("----------------------");
        }
    }

    private static void newGroup(Statement stmt) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите название группы: ");
        String group = scanner.nextLine();
        //String sql = "INSERT INTO `group`(name) VALUES('"+group+"')";
        String sql = String.format("INSERT INTO `group`(name) VALUES('%s')", group);
        stmt.executeUpdate(sql);
    }

    private static void newStudent(Statement stmt) throws SQLException {
        String[] student = new String[5];
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < student.length; i++) {
            System.out.print("Введите имя, фамилию, возраст, майл и id групы: ");
            student[i] = scanner.nextLine();
        }
        String sql = String.format("INSERT INTO student(first_name,last_name,age,email,group_id) VALUES('%s','%s','%s','%s','%s')", student);
        stmt.executeUpdate(sql);
    }

    private static void initDatabase(Statement stmt) throws SQLException {
        System.out.println(DROP_STUDENT);
        stmt.execute(DROP_STUDENT);

        System.out.println(CREATE_STUDENT);
        stmt.execute(CREATE_STUDENT);

        System.out.println(DROP_GROUP);
        stmt.execute(DROP_GROUP);

        System.out.println(CREATE_GROUP);
        stmt.execute(CREATE_GROUP);

        System.out.println(ALTER_STUDENT);
        stmt.execute(ALTER_STUDENT);
    }
}